# BuildStream Log Analyser

This is a tool to parse log output from BuildStream, for the purposes of performance analysis.

The current version of BuildStream (version 1.1.0) does not have millisecond timing or wallclock time, so this format is expected to be improved in the future to include these. This means the log analyser will also need to be improved to take account of this. At the moment, the analyser understands millisecond timing but not wallclock time.

Input can be supplied as the first command-line argument or on standard input. The output is given as a JSON file on standard out while log messages are printed on standard error.

The JSON file format is subject to change based on feedback from users.
