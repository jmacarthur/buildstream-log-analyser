#!/usr/bin/env python3

# BuildStream log analyser

import click
import json
import logging
import re
import sys

logging.basicConfig(level=logging.DEBUG)


class LogLine(object):
    def __init__(self, hours, minutes, seconds, ms, fields, action, description):
        self.hours = int(hours)
        self.minutes = int(minutes)
        self.seconds = int(seconds)
        self.ms = int(ms)
        self.fields = fields
        self.action = action
        self.description = self.description_converter(description)
    def time_seconds(self):
        return (self.hours * 3600 + self.minutes * 60 +
                self.seconds + self.ms * 0.000001)
    def description_converter(self, old_description):
        description = old_description.strip('[] ')
        if description == "Build Complete":
            return "Overall build time"
        if description.startswith('Fetching '):
            return "Fetching"
        if re.match("^\S*\.log$", description):
            return "Overall time"
        return description

class LogFileProcessor(object):
    def __init__(self):
        self.loglines = []

    def process_timed_log_line(self, hours, minutes, seconds, ms, fields, line):
        # Look for an action in the remainder of the line
        m = re.search(' ([A-Z]+) ', line)
        action = "UNDEFINED"
        if m:
            action = m.group(1)
            line = line[m.end():]
        # Anything left on the line is the 'description'
        description = line.strip()
        log = LogLine(hours, minutes, seconds, ms, fields, action, description)
        self.loglines.append(log)
        logging.debug("{} Action: '{}', description '{}'".format(fields, action, description))

    def process_log_line(self, line):
        fields = []
        while True:
            m = re.search('^\[[^\]]*\]', line)
            if not m:
                break
            if m.start() != 0:
                break
            # Strip off the leading bracketed field
            fields.append(m.group(0))
            line = line[m.end():]
        if len(fields) > 0:
            # Is the first field a time field?
            m = re.match('\[(\d\d):(\d\d):(\d\d)(\.\d{6})?\]', fields[0])
            if m:
                (hours, minutes, seconds) = (m.group(1), m.group(2), m.group(3))
                if m.group(4):
                    ms = m.group(4)[1:]
                else:
                    ms = 0
                self.process_timed_log_line(hours, minutes, seconds, ms, fields, line)

    def summarise_by_description(self, analysis_results):
        totals = {}
        for l in self.loglines:
            if l.description.endswith(".log"):
                continue
            description = l.description
            if description.startswith("Fetching"):
                description = "Fetching"
            if description in totals:
                totals[description] += l.hours*3600+l.minutes*60+l.seconds
            else:
                totals[description] = l.hours*3600+l.minutes*60+l.seconds
        keys_sorted_by_time = list(totals.keys())
        keys_sorted_by_time.sort(key=lambda x: totals[x])
        for k in keys_sorted_by_time:
            analysis_results[k] = totals[k]


def process_log_lines(file_handle):
    processor = LogFileProcessor()

    while True:
        line = file_handle.readline()
        if line == "":
            break
        processor.process_log_line(line.strip())


    # Now produce a json file...
    analysis_results = {
        "summary": {}
        }

    processor.summarise_by_description(analysis_results['summary'])

    for l in processor.loglines:
        if(len(l.fields)>2):
            package_name = l.fields[2].strip('][ ')
            if package_name not in analysis_results:
                analysis_results[package_name] = {}
            analysis_results[package_name][l.description] = l.time_seconds()
    print(json.dumps(analysis_results, sort_keys=True, indent=4))

@click.command()
@click.option('--example', default=1, help='Example option')
@click.argument('filename', required=False)
def main(example, filename):
    """Analyses BuildStream log files."""
    f = None
    try:
        if filename is None:
            f = sys.stdin
        else:
            f = open(filename, "rt")
        process_log_lines(f)
    finally:
        if f:
            f.close()


if __name__ == "__main__":
    main()
